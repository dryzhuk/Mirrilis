Test template
--
- View site: http://dryzhuk.gitlab.io/Mirrilis/
- Version 1.0
- Author: Roman Dryzhuk 


## Configuration:

- Default
- GitLab CI: [`.gitlab-ci.yml`](https://gitlab.com/dryzhuk/Mirrilis/blob/master/.gitlab-ci.yml)

## More Info:
- https://t.me/dryzhuk